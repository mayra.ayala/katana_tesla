/* Proyecto Katana js  */ 

/*linea para git */
let statusx
let statusArry
let boxpoint=[]
let desicion=[]
let pointsArray=[]
let cadenadedatos
let pointsAll

let fullimage = document.getElementById('CanvasFHD')
let fullimagectx = fullimage.getContext('2d')

let recortito= document.getElementById('Canvascut')
let recortitoctx = recortito.getContext('2d')


let capturap30 = document.getElementById('imagen_p30')
let capturap30tcx = capturap30.getContext('2d')

let capturap20 = document.getElementById('imagen_p20')
let capturap20tcx = capturap20.getContext('2d')

let capturap10 = document.getElementById('imagen_p10')
let capturap10tcx = capturap10.getContext('2d')


//************************************************************************************** Imagenes de prueba para la funcion loadcapturas*/
let image = new Image()
image.src = "/img/p30.png"

let image2 = new Image()
image2.src = "/img/p20.png"

let image3 = new Image()
image3.src = "/img/p20.png"

//*************************Socket block */
const socket = io();

socket.on('Sequence_start',function(infoplc){//pg migrated
	
	if (infoplc!= 0) {
        cadenadedatos = infoplc.toString()

        Sequence()//Activa bandera para continuar

        console.log("Start test sequence");
       // console.log(typeof(data))
        //console.log(infoplc)
       //console.log(pn)
        }
        else{	
        console.log("Algo salio mal en el backend");
        }});
    
function plc_response(boxpoint){ //El Array boxpoint guarda la equivalencia del punto, cuando vale pass o cuando vale fail
            return new Promise(async resolve =>{
               boxpoint=
               "&P3011"+","+boxpoint[1]+
               "&P3012"+","+boxpoint[2]+
               "&P2011"+","+boxpoint[3]+
               "&P2012"+","+boxpoint[4]+
               "&P1011"+","+boxpoint[5]+
               "P1012"+","+boxpoint[6]+"#"
                
               
               console.log(boxpoint)
                socket.emit('plc_response',boxpoint)
                resolve('resolved')})
        }






//************************************************************************************** Metricos*/

//***********Canvas de grafica barras */ 
const bar_ctx = document.getElementById('bar').getContext('2d');
const bar = new Chart(bar_ctx, {
    type: 'bar',
    data: {
        labels: ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes'],
        datasets: [{
            label: 'Yield x Day',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options : {
        scales: {
            x: {
                grid: {
                    display:false
               }
            },
            y: { beginAtZero: true,
                    grid: {
                         display:false
                    }
            }                                                         
        }
    }
});

/************Canvas de grafica lineal */

let line_ctx = document.getElementById('linea').getContext('2d');
let linea = new Chart(line_ctx, {
  type: 'line',
  data: {
      labels: ['6am', '7am', '8am', '9am', '10am', '11am','12pm','1pm','2pm','3pm','4pm','5pm','6pm'],
      datasets: [{
          label: 'Yield x Hr',
          data: [12, 19, 3, 5, 4, 3, 12, 6, 3, 1, 8, 3, 2],
          backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(153, 102, 255, 0.2)',
              'rgba(255, 159, 64, 0.2)',
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)'
          ],
          borderColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(153, 102, 255, 1)',
              'rgba(255, 159, 64, 1)', 
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)'
          ],
          borderWidth: 1
      }]
  },
  options: {
      scales: {
                x: {
                    grid: {
                        display:false
                    }
                },
                y: {
                    beginAtZero: false,
                    grid: {
                        display:false
                    }
                }
      }
  }
});

/************Canvas de grafica pay*/ 
 let pay_ctx = document.getElementById('pay').getContext('2d');
 let pay = new Chart(pay_ctx, {  
  
  type: 'polarArea',
  data: {
      labels: ['P3011', 'P3012', 'P2011', 'P2012', 'P1011', 'P1012'],
      datasets: [{
          label: '# of Votes',
          data: [12, 19, 3, 5, 0, 3],
          backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(153, 102, 255, 0.2)',
              'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(153, 102, 255, 1)',
              'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
      }]
  },
  options: { // Inserccion de objetos para modificar diseño de graficas
      scales: {
          xAxes: {
              beginAtZero: true,
              grid: {
                display:false
            }
          }, 
          r: {
            grid: {
                display:true
            }
        }

          
      }
  }
});
 
//************************************************************************************** Secuencia de prueba */
/*************Secuencia   */ 
async function Sequence(){
    document.getElementById('boton').style.visibility = "hidden"

    //await st(cadenadedatos)

    boxpoint=[] // Reinicia valor para retrabajar punto
    desicion=[]
    
    for (point=1; point<7; point++){  
    await open_cam(point)
    canbughide()
    await captureimage()
    await recorta(point)
    } // Cierre de for puntos
    await stopcam()
    await evaluaArray() // funcion se coloca fuera de for para evaluar toda la cadena
    await plc_response(boxpoint)
    setTimeout(function fire(){location.reload()},5000);// temporizador para limpiar pantalla
    
}

function st(st){
    return new Promise(async resolve =>{
        elementst= document.getElementById('st')
        //console.log(station)
        elementst.innerHTML= "Serial: "+st+""
    resolve('resolved')})
}



//************************************************************************************** Funciones de procesamiento de imagenes */
async function recorta(point){ // Recorte de canvas 
    return new Promise(async resolve =>{ 
        switch(point){
            case 1:
                // imagen No1
                // P3011
                recortitoctx.drawImage(fullimage,223,311,420,420,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height) // coordenada y tamaño de recorte en el canvas 
                capturap30tcx.drawImage(fullimage, 8, 285, 1913, 483, 0, 0, 318, 90) // Ajuste de imagen Cropping y resize
                boxShadow(point)
                await mlinspector(point)
                pointstatus(point, statusx)
                allpoints(1,statusx)
                break;
           case 2:
                // P3012
                recortitoctx.drawImage(fullimage,1272,309,420,420,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
                await mlinspector(point)
                pointstatus(point, statusx)
                allpoints(2,statusx)
                break;
                // imagen No2 con diferente medida por altura
             case 3: 
                // P2011
                recortitoctx.drawImage(fullimage,340,289,420,420,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
                capturap20tcx.drawImage(fullimage, 184, 315, 1739, 435, 0, 0, 321, 90)
                boxShadow(point)
                await mlinspector(point)
                pointstatus(point, statusx)
                allpoints(3,statusx)
                break;
            case 4: 
                // P2012
                recortitoctx.drawImage(fullimage,1302,324,420,420,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
                await mlinspector(point)
                pointstatus(point, statusx)
                allpoints(4,statusx)
                break;
                // imagen No3
            case 5:
                // P1011
                recortitoctx.drawImage(fullimage,104,326,420,420,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
                capturap10tcx.drawImage(fullimage, 1, 243, 1916, 565, 0, 0, 321, 90)
                boxShadow(point)
                await mlinspector(point)
                pointstatus(point, statusx)
                allpoints(5,statusx)
                break;
            case 6: 
                // P1012
                recortitoctx.drawImage(fullimage,1424,318,420,420,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
                await mlinspector(point)
                pointstatus(point, statusx)
                allpoints(6,statusx)
                break;
                default:
        }
       
        resolve('resolved')})
}

function puntos(point){
    pointsArray[point] = pointsAll
    console.log(pointsAll)

}


function boxShadow(point){
    if(point == 1){
    document.getElementById("imagen_p30").style.boxShadow = "10px 20px 30px rgba(96, 95, 99, 0.75)"
    }
    if(point == 3){
    document.getElementById("imagen_p20").style.boxShadow = "10px 20px 30px rgba(96, 95, 99, 0.75)"
    }
    if(point == 5){
    document.getElementById("imagen_p10").style.boxShadow = "10px 20px 30px rgba(96, 95, 99, 0.75)"
    }
}

//************************************************************************************** Funciones para dibujar cuadros de status */

function pointstatus(point,statusx){ // funcion que pinta verde o rojo cuadro analizado 
    switch(point){
    case 1:
        if(statusx == "1"){}
        if(statusx == "0"){}
        break
    case 2:
        if(statusx == '1'){}
        if(statusx == '0'){}
        break
    case 3:
        if(statusx == '1'){}
        if(statusx == '0'){}
        break
    case 4:
        if(statusx == '1'){}
        if(statusx == '0'){}
        break
    case 5:
        if(statusx == '1'){}
        if(statusx == '0'){}
        break
    case 6:
        if(statusx == '1'){}
        if(statusx == '0'){}
        break
    default:
    }
}

function p3011_v(){
    let capturap30 = document.getElementById('imagen_p30')
    let capturap30tcx = capturap30.getContext('2d')

    capturap30tcx.strokeStyle = "#39FF14";
    capturap30tcx.lineWidth = 3;
    capturap30tcx.strokeRect(9, 5, 90, 80)
}
function p3012_v(){
    let capturap30 = document.getElementById('imagen_p30')
    let capturap30tcx = capturap30.getContext('2d')

    capturap30tcx.strokeStyle = "#39FF14";
    capturap30tcx.lineWidth = 3;
    capturap30tcx.strokeRect(225, 7, 90, 80)
}
function p2011_v(){
    let capturap20 = document.getElementById('imagen_p20')
    let capturap20tcx = capturap20.getContext('2d')  

    capturap20tcx.strokeStyle = "#39FF14";
    capturap20tcx.lineWidth = 3;
    capturap20tcx.strokeRect(0, 5, 90, 80)
}
function p2012_v(){
    let capturap20 = document.getElementById('imagen_p20')
    let capturap20tcx = capturap20.getContext('2d')  

    capturap20tcx.strokeStyle = "#39FF14";
    capturap20tcx.lineWidth = 3;
    capturap20tcx.strokeRect(229, 7, 90, 80)
}
function p1011_v(){
    let capturap10 = document.getElementById('imagen_p10')
    let capturap10tcx = capturap10.getContext('2d')

    capturap10tcx.strokeStyle = "#39FF14";
    capturap10tcx.lineWidth = 3;
    capturap10tcx.strokeRect(7, 2, 90, 80)
}
function p1012_v(){
    let capturap10 = document.getElementById('imagen_p10')
    let capturap10tcx = capturap10.getContext('2d')

    capturap10tcx.strokeStyle = "#39FF14";
    capturap10tcx.lineWidth = 3;
    capturap10tcx.strokeRect(231, 7, 90, 80)
}
function p3011_r(){
    let capturap30 = document.getElementById('imagen_p30')
    let capturap30tcx = capturap30.getContext('2d')

    capturap30tcx.strokeStyle = "#ff0000";
    capturap30tcx.lineWidth = 3;
    capturap30tcx.strokeRect(9, 5, 90, 80)
}
function p3012_r(){
    let capturap30 = document.getElementById('imagen_p30')
    let capturap30tcx = capturap30.getContext('2d')  

    capturap30tcx.strokeStyle = "#ff0000";
    capturap30tcx.lineWidth = 3;
    capturap30tcx.strokeRect(214, 5, 90, 80)

}
function p2011_r(){
    let capturap20 = document.getElementById('imagen_p20')
    let capturap20tcx = capturap20.getContext('2d')  

    capturap20tcx.strokeStyle = "#ff0000";
    capturap20tcx.lineWidth = 3;
    capturap20tcx.strokeRect(8, 5, 90, 80)
}
function p2012_r(){
    let capturap20 = document.getElementById('imagen_p20')
    let capturap20tcx = capturap20.getContext('2d')  

    capturap20tcx.strokeStyle = "#ff0000";
    capturap20tcx.lineWidth = 3;
    capturap20tcx.strokeRect(229, 7, 90, 80)
}
function p1011_r(){
    let capturap10 = document.getElementById('imagen_p10')
    let capturap10tcx = capturap10.getContext('2d')  

    capturap10tcx.strokeStyle = "#ff0000";
    capturap10tcx.lineWidth = 3;
    capturap10tcx.strokeRect(7, 2, 90, 80)
}
function p1012_r(){
    let capturap10 = document.getElementById('imagen_p10')
    let capturap10tcx = capturap10.getContext('2d')  

    capturap10tcx.strokeStyle = "#ff0000";
    capturap10tcx.lineWidth = 3;
    capturap10tcx.strokeRect(231, 7, 90, 80)
}

//************************************************************************************** Funciones para localizar los Arrays*/
function allpoints(pot,statusl){ 
    boxpoint[pot] = statusl// Array guarda el valor de cada punto analizado 
}

function evaluaArray(){
    return new Promise(async resolve =>{ 
    let evalua 
    desicion[0]=boxpoint[1]
    desicion[1]=boxpoint[2]
    evalua = desicion.some((e) => e == "0") 
    if(evalua == true){ evalua = "Fail"}else{ evalua = "1"} 
    //console.log("Desicion final de evalua:",evalua)

    desicion[0]=boxpoint[3]
    desicion[1]=boxpoint[4]
    evalua = desicion.some((e) => e == "0") 
    if(evalua == true){ evalua = "Fail"}else{ evalua = "1"} 

    desicion[0]=boxpoint[5]
    desicion[1]=boxpoint[6]
    evalua = desicion.some((e) => e == "0") 
    if(evalua == true){ evalua = "0"}else{ evalua = "1"} 

    let resultadofinal = boxpoint.some((e) => e == "0")
    if (resultadofinal== false){
        pass()
    }else{
        fail()
    }
    //console.log(boxpoint)
    //console.log(resultadofinal)
    resolve('resolved')}) // fin de promesa
}

//************************************************************************************** Funciones de debug*/
let calis = new Image()// Variable utilizada por switchpic

function loadcalis(fotox){//Funcion Carga la imagen del modelo 
    switchpic(fotox)
    setTimeout(function dibuja(){
        fullimagectx.drawImage(calis, 0, 0, calis.width, calis.height, 0, 0, fullimagectx.canvas.width, fullimagectx.canvas.height)
        canbughide()
    },300)   
}
function switchpic(name){
    calis.src = "/img/Camara/"+name+".jpg"
}
function canbughide(){ // funcion para esconder los canvas 
    return new Promise(async resolve =>{  
    document.getElementById('CanvasFHD').style.visibility = "hidden"
    //document.getElementById('Canvascut').style.visibility = "hidden"
    resolve('resolved')});
}
function canbugshow(){ // funcion para ver los canvas 
    document.getElementById( 'Canvascut' ).style.visibility = "visible"

}
function loadcapturas(){ // funcion para cargar imagenes de prueba
    capturap30tcx.drawImage(image, 0, 0, image.width, image.height, 0, 0, capturap30tcx.canvas.width, capturap30tcx.canvas.height);
    capturap20tcx.drawImage(image2, 0, 0, image2.width, image2.height, 0, 0, capturap20tcx.canvas.width, capturap20tcx.canvas.height);
    capturap10tcx.drawImage(image3, 0, 0, image3.width, image3.height, 0, 0, capturap10tcx.canvas.width, capturap10tcx.canvas.height);
}

//document.getElementById("myinput").onkeydown = function() {Sequence()}; // inicia la secuencia cuando detecta un texto presionando enter

function search(event) {
    let  tecla = event.key

    if(tecla === 'Enter') {
    event.preventDefault()     
    console.log("funciona secuencia")    
    Sequence()
    }
}


//************************************************************************************** Funciones de camara*/
/*Seccion de camaras  */
function open_cam(point){// Resolve de 2 segundos

	return new Promise(async resolve =>{
    let camid
    
    if(point == 1) {camid="c5bab9f5134f5c1ef2482ddab6e894247d5eab7b026c6d084632c1a12ddfcde6"} // Camara 30
    if(point == 5) {camid="fb2057d82c74b91f0bfc3eefc5983015a5620612b1c99091f0fd186825700a05"} // Camara 10
    if(point == 3) {camid="67ffd4cbfa9a67b549cc1662677800420ea4d2deb466d794b68fca2480ad2326"} // Camara 20
    
    //ID cam 624780613a62307e2e76ea163312e4a55005363356fdb70be20f67f3ace503dc
    //ID cam 5 be4c578873f44d98a15973d3ae123b63e2ed3d386472472701145e906ff0c78e
    //ID cam 1 57f7d5859856ce7265266b9c77e1e12ae786cb06ac83be55a04b028eb27ffd85
    //ID cam 3 80b54a6dd9e95c2f00d446737187e6a77cfdea3671ecf9d86a5b58f5e20c5d79
    //ID cam 1 0036c6d37442abdeaff0ae9423bf18a01c2e779ebcf98b2d73371e28ed80c7e9
    //ID cam 3 8c229d23f265e4c41f93d33d74f5a19e2b219d994c2665d134896c6a589e7585
    //ID cam 5  5624780613a62307e2e76ea163312e4a55005363356fdb70be20f67f3ace503dc
    const video = document.querySelector('video')
    const vgaConstraints = {    
                        
        video:{             
            width: { ideal:1080 },
            "frameRate": 30,
            "resizeMode": "crop-and-scale",
             deviceId:camid}
            }
        

        let objetomedia = navigator.mediaDevices.getUserMedia(vgaConstraints)

        await objetomedia.then((stream) => {video.srcObject = stream})

        setTimeout(function fire(){resolve('resolved');console.log("Open cam fired")},500)
    })//Cierra Promise principal
}
function captureimage(){// Resolve de 2 segundos
        return new Promise(async resolve =>{        
            
           let image = document.getElementById( 'CanvasFHD' );
           let contexim2 = image.getContext( '2d' );	

           var video = document.getElementById("video");     
           
           w = image.width;
           h = image.height;
            
           contexim2.drawImage(video,0,0,image.width,image.height);
            //var dataURI = canvas.toDataURL('image/jpeg');
        	//setTimeout(function fire(){resolve('resolved');},2000);//Temporal para programacion de secuencia
        	resolve('resolved')});
}
function mapcams(){ // Mapeo de camaras para identificarlas 
    navigator.mediaDevices.enumerateDevices()
    .then(devices => {
    const filtered = devices.filter(device => device.kind === 'videoinput');			
                         console.log('Cameras found',filtered);
                          });
}
function stopcam(){
    return new Promise(async resolve =>{
        const video = document.querySelector('video');
        // A video's MediaStream object is available through its srcObject attribute
        const mediaStream = video.srcObject;
        // Through the MediaStream, you can get the MediaStreamTracks with getTracks():
        const tracks = mediaStream.getTracks();
        //console.log(tracks);
        // Tracks are returned as an array, so if you know you only have one, you can stop it with: 
        //tracks[0].stop();
        // Or stop all like so:
        tracks.forEach(track => {track.stop()})//;console.log(track);
    setTimeout(function fire(){resolve('resolved');},1000);
    });//Cierra Promise principal
}
function snapshot(){//Back end sent URI,SN? & point?
	return new Promise(async resolve =>{
		var dataURI = fullimage.toDataURL('image/jpeg');
		savepic(dataURI,snfile,point); //savepic(dataURI,point);
		//console.log("Pic Sent--"+sn+"--"+point);
		//setTimeout(function fire(){resolve('resolved');},2000);//Temporal para programacion de secuencia
		resolve('resolved')});
}	


//****************************************** Backend call functions

function  plckatana(p){
	const socket = io();		
	socket.emit('plckatana',p);		
}

//************************************************************************************** IA*/
const classifier = knnClassifier.create() //Contiene librerias para poder funcionar 

ml()

async function mlinspector(pot){
    return new Promise(async resolve =>{ // inicio de promesa 
        let x = 0
        let y = 0
        let w = 0
        let h = 0
        if(pot == 1){x = 223, y =311, w = 420, h = 420} // Referencia de coordenadas a inspeccionar
        if(pot == 2){x = 1272, y = 309, w = 420, h = 420}
        if(pot == 3){x = 340, y = 289, w = 420, h = 420}
        if(pot == 4){x = 1302, y = 324, w = 420, h = 420}
        if(pot == 5){x = 104, y = 326, w = 420, h = 420}
        if(pot == 6){x = 1424, y = 318, w = 420, h = 420}

        switch(pot){
            case 1: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("Samples",84) // Argumentos nuca y vari
            await predict(recortito)
            break;
            case 2: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("Samples",84) // Argumentos nuca y vari
            await predict(recortito)
            break;
            case 3: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("Samples",84) // Argumentos nuca y vari
            await predict(recortito)
            break;
            case 4: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("Samples",84) // Argumentos nuca y vari
            await predict(recortito)
            break;
            case 5: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("Samples",84) // Argumentos nuca y vari
            await predict(recortito)
            break;
            case 6: 
            recortitoctx.drawImage(fullimage,x,y,w,h,0,0,recortitoctx.canvas.width,recortitoctx.canvas.height)
            await call("Samples",84) // Argumentos nuca y vari
            await predict(recortito)
            break;
            default:
        }
    resolve('resolved')})  
}
async function ml() {
    // Load the model.
    net = await mobilenet.load(); // red neuronal echa, ejemplo, ya esta entrenada  
    console.log('Neural network load success...');
}

/************************ Variable 'nuca' representa el nombre del documento JSON variable */
/************************ Variable 'vari' representa numero de muestras de la red neuronal*/
function call(nuca,vari){ 
    return new Promise(async resolve =>{
      let data;
      load("/ml/neuralnetworks/"+nuca+".json", function(text){ //C:/Users/juan_moreno/myapp/public/Nokia Vision
      data = JSON.parse(text);	
      //*****************************COnvertir a tensor
      //Set classifier
      Object.keys(data).forEach((key) => {
        data[key] = tf.tensor2d(data[key],[vari,1024]); //Shape es [# imagenes,dimension pix] ,[19,1024]
      });
      //Set classifier
      //console.log(data);
      classifier.setClassifierDataset(data)
           }) // fin de load 
           setTimeout(function fire(){resolve('resolved')},500); 
       }) // fin de promesa 
}
function load(file, callback) {  // funcion que se encarga de llamar Json con el nuevo entrenamiento 
        //Can be change to other source	
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
               callback(rawFile.responseText);
        }
     }
    rawFile.send(null)
}
async function predict(){ // wonka es variable
        return new Promise(async resolve =>{	
        // Get the activation from mobilenet from the webcam.
        const activation = net.infer(recortito, 'conv_preds');
        // Get the most likely class and confidences from the classifier module.
        const result = await classifier.predictClass(activation); // Clasifica, decide depende de la imagen lo que le va a poner 
        const classes = ['Pass', 'Fail'];
        
        if(classes[result.label] == 'Pass'){
            statusx = "1" 
        }else{
            statusx = "0"
        }
            console.log(classes[result.label])
           // console.log(result.confidences[result.label]*100)
        resolve('resolved')
        });//Cierra Promise						
}//if logic end


  
