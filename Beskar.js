/* Server para Katana "Nombre opcional"  */ 
//-----* Express inicia servidor / carpeta raiz
// Codigo para el server 
const express=require('express');
const app=express();

app.use(express.static(__dirname))
const server=app.listen(4444, () =>{
    console.log('server activated !')
})

//*----- TCP/IP PLC 

var io = require('socket.io')(server); //Bind socket.io to our express server.

let plc_endresponse=0
io.on('connection', (socket) => {//Un Socket para todos los requerimientos a postgres

    socket.on('plckatana', async function (p){ // comunicacion con la funcion de javascript
        plc(p);
    });

    socket.on('plc_response', function(result_matrix){
             plcdatasender(result_matrix) 
    });

});//Close io.on
//************************************************************** Server, espera algun dato de plc para arranque de secuencia  */

var net = require('net');

var tcpipserver = net.createServer(function(connection) { 
   console.log('TCP client connected');
   //connection.on('end', function() {
      //console.log(Buffer.toString());  
   //});
   connection.write('Handshake ok!\r\n');
   //connection.pipe(connection);

   //Funcion para imprimir la cadena que le envianconsole.log(data)
   //connection.on('data', function(data) { console.log(data.toString())});
   connection.on('data', function(data) { io.emit('Sequence_start',data.toString());console.log("Analisis in process...");})
   
  //console.log('Received bytes: ' + data);
  
  //Inicia la secuencia

//Responde a PLC cuando termine inspeccion
setTimeout(function respuesta(){
    estadoconexion = connection.readyState
    console.log("Comunicacion con el plc :"+connection.readyState)
    
       if (estadoconexion == 'closed' ){
           console.log("Puerto de PLC cerrado reintento en 1min..."  )
       }
       if (estadoconexion == 'open'){
              connection.write(plc_endresponse)
       }

  },55000)

  })

function plcdatasender(result_matrix) {
	matrixtostring=result_matrix.toString()
	plc_endresponse=matrixtostring
}


tcpipserver.listen(40000, function() { 
    console.log('PLC Port 40000 listening...');
 })
